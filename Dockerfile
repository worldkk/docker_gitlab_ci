FROM node:latest
WORKDIR /toyota-service
COPY package.json ./
RUN npm install
COPY . .
CMD ["node", "index.js"]
